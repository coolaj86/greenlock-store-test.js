'use strict';

//var tester = require('greenlock-store-test');
var tester = require('./');

var store = require('greenlock-store-memory').create({});

tester.test(store).then(function () {
  console.info("PASS");
}).catch(function (err) {
  console.error("FAIL");
  console.error(err);
  process.exit(20);
});
